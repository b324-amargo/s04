<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<title>S04: Access Modifiers and Encapsulation</title>
</head>
<body>
	<h1>Building</h1>
	<p><?php echo "The name of the building is ".$building->getName()."."; ?></p>
	<p><?php echo "The ".$building->getName()." has ".$building->getFloor()." floors."; ?></p>
	<p><?php echo "The ".$building->getName()." is located at ".$building->getAddress()."."; ?></p>
	<p><?php echo $building->setName("Caswynn Complex"); ?></p>
	<p><?php echo "The name of the building has been changed to ".$building->getName()."."; ?></p>

	<h1>Condominium</h1>
	<p><?php echo "The name of the condominium is ".$condo->getName()."."; ?></p>
	<p><?php echo "The ".$condo->getName()." has ".$condo->getFloor()." floors."; ?></p>
	<p><?php echo "The ".$condo->getName()." is located at ".$condo->getAddress()."."; ?></p>
	<p><?php echo $condo->setName("Enzo Tower"); ?></p>
	<p><?php echo "The name of the condominium has been changed to ".$condo->getName()."."; ?></p>

</body>
</html>