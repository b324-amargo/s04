<?php 

// Access Modifiers
	class Building {
		protected $name;
		protected $floors;
		protected $address;

		public function __construct($name, $floors, $address){
			$this->name = $name;
			$this->floors = $floors;
			$this->address = $address;
		}

		// =======================

		public function getName(){
			return $this->name;
		}

		public function setName($name){
			$this->name = $name;
		}

		// =======================

		public function getFloor(){
			return $this->floors;
		}

		// =======================

		public function getAddress(){
			return $this->address;
		}

	}
	// parent class

	$building = new Building("Caswynn Building", 8, "Timog Avenue, Quezon City, Philippines");

	// =======================

	class Condominium extends Building{

	}
	// child class

	$condo = new Condominium("Enzo Condo", 5, "Buendia Avenue, Makati City, Philippines");
	
?>